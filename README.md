# A Bridge for the Wonde PHP Client

## Installation

You can install the package via composer:

```bash
composer require schoolspider/laravel-wonde
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="SchoolSpider\LaravelWonde\WondeServiceProvider" --tag="config"
```

This is the contents of the published config file:

```php
return [
    'token' => env('WONDE_TOKEN', null)
];
```

## Usage

```php
$wonde = new SchoolSpider\Wonde();
$wonde->schools->all();
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security

If you discover any security related issues, please email freek@spatie.be instead of using the issue tracker.

## Credits

-   [Simon Morris](https://github.com/smorris1709)
-   [All Contributors](../../contributors)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
