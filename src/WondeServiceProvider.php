<?php

namespace SchoolSpider\LaravelWonde;

use Illuminate\Support\ServiceProvider;
use SchoolSpider\LaravelWonde\Exceptions\InvalidConfiguration;
use Wonde\Client;

class WondeServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/wonde.php' => config_path('wonde.php'),
            ], 'laravel-wonde');
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/wonde.php', 'wonde');

        $this->app->singleton('wonde', function ($app) {
            $token = config('wonde.token');

            if (!$token) {
                InvalidConfiguration::tokenNotSpecified();
            }

            return new Client(config('wonde.token'));
        });
    }

    protected function guardAgainstInvalidConfiguration(array $wondeConfig = null)
    {
        if (empty($wondeConfig['token'])) {
            throw InvalidConfiguration::tokenNotSpecified();
        }
    }
}
