<?php

namespace SchoolSpider\LaravelWonde\Exceptions;

use Exception;

class InvalidConfiguration extends Exception
{
    public static function tokenNotSpecified()
    {
        return new static('There was no wonde token specified. You must provide a valid token to interact with Wonde API.');
    }
}
