<?php

namespace SchoolSpider\LaravelWonde;

use Illuminate\Support\Facades\Facade;

class WondeFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'wonde';
    }
}
