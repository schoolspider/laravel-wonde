# Security Policy

## Reporting a Vulnerability

If you discover any security related issues, please email security@schoolspider.co.uk instead of using the issue tracker.
